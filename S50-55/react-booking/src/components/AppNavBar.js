import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import { Fragment, useState, useEffect, useContext} from 'react';

import { Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar()
{

	const { user } = useContext(UserContext)
	
	/*window.addEventListener('loginEvent', () => {
		setUser(localStorage.getItem("email"));
    	// ...
	});
	
	window.addEventListener('logoutEvent', () => {
		setUser(localStorage.getItem("email"));
    	// ...
	})*/
	//Doesn't work if open parenthesis is below return
	//e.g
	//return
	//(
	//
	//)
	return(
		<Navbar bg="light" expand="lg">
	      <Container fluid>
	        <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="me-auto">

	            <Nav.Link as={Link} to="/">Home</Nav.Link>
	            <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
	            {(user.id !== null)?	

	            	<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
	            	:
	            	<Fragment>

		            <Nav.Link as={Link} to="/login">Login</Nav.Link>
		            <Nav.Link as={Link} to="/register">Register</Nav.Link>
	            	
	            	</Fragment>

	            }
	            
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	);
	
}
