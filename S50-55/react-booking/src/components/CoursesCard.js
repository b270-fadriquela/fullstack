import {Row, Col, Card, Button} from 'react-bootstrap';
import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


//View
export default function CoursesCard({data}) {



	const {_id, name, description, price, slots} = data;

	return(

		<Card className="cardCourses mt-2">
		    <Card.Body>
		      <Card.Title>{name}</Card.Title>
		      <Card.Subtitle className="mb-2 text-muted">Php {price}</Card.Subtitle>
		      <Card.Text>{description}</Card.Text>
		      <Card.Text>{slots} Remaining</Card.Text>

		      <Button  variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
		    </Card.Body>
		  </Card>

		
	)

}