import {Row, Col, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom';
import { Fragment} from 'react';
import { Navigate } from "react-router-dom";

function Banner({bannerData}){

	console.log(bannerData.route);

	const route = bannerData.route;
	const routeName = bannerData.routeName;
	function handleClick() {
	}


	return(
		<Row>
			<Col className="p-5">
				<h1>{bannerData.title} </h1>
				


				<p className="">{bannerData.subTitle} <Link as={Link} to={route}>{routeName}</Link></p>

				
				{(bannerData.showButton === true)?	

	            	
	            	<Button variant="primary" >Enroll Now!</Button>
	            	:
	            	<Fragment>

	            	
	            	</Fragment>

	            }

			</Col>
		</Row>


	)
}






export default Banner;