import './App.css'
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';

import {Fragment} from 'react'
import {Container} from 'react-bootstrap'

import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Routes }  from 'react-router-dom'

import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/Error';
import Welcome from './pages/Welcome';
import { useState, useEffect} from 'react';
import { UserProvider } from './UserContext';





function App() {

  const [user, setUser] = useState({email: localStorage.getItem('email')});
  const [userName, setUserName] = useState({name: ""});
  
  useEffect(() => {
    console.log(user);
  }, [user])

  useEffect(()=>{
      

      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
               headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
       })
      .then(res => res.json())
      .then(data => {

          if(data._id !== undefined){
              setUser({
                  id:data._id,
                  isAdmin: data.isAdmin
              });

          }
          else{
            setUser({
                id:null,
                isAdmin: null
            });

          }
          
      })
      .catch(rejected => {
          console.log(rejected);
      });

  }, [])

  const unsetUser = () => {
    // window.dispatchEvent(new Event("logoutEvent"));
    localStorage.clear();
  }

  return ( 
  <UserProvider value={{user, setUser, unsetUser, userName,setUserName}}>
      <Router>
      <Container fluid>
        <AppNavBar />
        <Routes>
          <Route path ="/" element ={<Home />} />
          <Route path ="/courses" element ={<Courses />} />
          <Route path ="/courses/:courseId" element ={<CourseView />} />
          <Route path ="/register" element ={<Register />} />
          <Route path ="/login" element ={<Login />} />
          <Route path ="/logout" element ={<Logout />} />
          <Route path="*" element={<PageNotFound />} />
          <Route path="/welcome" element={<Welcome />} />
        </Routes>

      </Container>

      </Router>
    </UserProvider>
   
  );
}

export default App;
