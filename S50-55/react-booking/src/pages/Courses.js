
import { Fragment, useEffect, useState } from 'react';
import coursesData from '../data/coursesData';
import CoursesCard from '../components/CoursesCard';

export default function Courses(){


	const [courses, setCourses] = useState([]);
		

	useEffect(()=>{

	     fetch(`${process.env.REACT_APP_API_URL}/courses`)
		    .then(res => res.json())
		    .then(data => {
		    	
		    	setCourses (data.map(course =>{

		    		return(
		    			<CoursesCard key={course._id} data={course} />
		    		)
		    	}))
		    })
		    .catch(rejected => {
		        console.log(rejected);
		    });

	}, [])

	return(
		<Fragment>
			 <h1>Courses</h1>
			 {courses}

		</Fragment>
	)


}