

import { Fragment, useState, useEffect, useContext, } from 'react';
import { Navigate, useLocation} from "react-router-dom";
import UserContext from '../UserContext';

//Show Welcome Page
//Transition to courses in 2 seconds


export default function Welcome ()  {

	const {user, userName} = useContext(UserContext);
	const [timerFinish, setTimerFinish] = useState(0);
		const [count, setCount] = useState(0);
		const location = useLocation();

	//componentWillMount()
	//


	const CountDown = () => {

		const [isTimerDone, setTimerState] = useState(false);

		useEffect(() => {
			if(count > 2)
			{
				setTimerState(true)
				setTimerFinish(1);
				console.log(timerFinish);
				return;
			}


			const timer = setTimeout(() => !isTimerDone && setCount(count+1),1e3)


				

			return () => {
				clearTimeout(timer)
			};

		}, [count])

	}

	CountDown();

	console.log(location.state.header);
	return(



		(user.id !== null)
		?
			<Fragment>
			<div className='p-5'>
			<h1> {location.state.header} {userName.name}! </h1>
			<p> {location.state.message}</p>
			</div>

			{/*start 2seconds*/}

			{
				timerFinish === 1
	         ?
	        	<Navigate to="/courses" /> 
	         :
	       		<h4 className='p-5'>{location.state.submessage} { 3 - count}</h4>
	    	}
	    	</Fragment>

		:

		<div>
	        <Navigate to="/" /> 
		</div>



	)

}