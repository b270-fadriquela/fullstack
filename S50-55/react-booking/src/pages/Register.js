import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext'

import Swal from 'sweetalert2';
import { Navigate } from "react-router-dom";

export default function Register() {

	const { user, setUser, setUserName, userName} = useContext(UserContext);

	const [ email, setEmail ] = useState("");
	const [ password1, setPassword1 ] = useState("");
	const [ password2, setPassword2 ] = useState("");

	const [ firstName, setFirstName ] = useState("");
	const [ lastName, setLastName ] = useState("");
	const [ mobileNo, setMobileNo ] = useState("");

	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {
		if(password1 === password2 && (password1 !== "" && password2 !== "" && email !== ""))
			setIsActive(true);
		else
			setIsActive(false)

	}, [password2 , password1, email])

	const verifyEmail = (e) => {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res=> res.json())
		.then(data => {

			if(data===true) {
				//Email Taken
				Swal.fire({
					title: "Email Already Taken!",
					icon: "error",
					text: "Please try again."
				})
			} else {
				//Email Available
				registerUser();
			}
		})
	}

	function registerUser(){

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNo: mobileNo,
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if(data===true) {
				//AutoLogin the User
				loginUser();
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	function loginUser(){
        
        //request REACT_APP_API_URL
       fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email:email,
                password:password1
            })

        })
        .then(res => res.json())
        .then(data => {

            if(data.accessToken !== undefined){
                //console.log("success");
                retrieveUserDetails(data.accessToken)
                localStorage.setItem('token', data.accessToken);


            }
            else{
                Swal.fire({
                    title:"Authentication Failed!",
                    icon: "error",
                    text: "Something went wrong"
                })
            }
            
        })
        .catch(rejected => {
            console.log(rejected);
        });

	}

	const retrieveUserDetails = (token) => { 

        setEmail("");
		setPassword1("");
		setPassword2("");
		setFirstName("");
		setLastName("");
		setMobileNo("");


            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
               headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(res => res.json())
            .then(data => {

                setUser({
                    id:data._id,
                    isAdmin: data.isAdmin
                });

                setUserName({
                    name:data.firstName
                })

                //go to welcome page?

            });
    };

    const welcomeData ={
        header:"Welcome new Student ",
        message:"It's a good day to learn ",
        submessage:"Moving to courses in a few seconds"
    }
 	 return (

 	 	(user.id !== null)
        ?
       <Navigate state={welcomeData} to="/welcome"/ >
        :

	    <Form className="p-2"onSubmit={(e) => verifyEmail(e)}>

	    	<Form.Group className="mb-3" controlId="userFirstName">
	    	  <Form.Label>First Name</Form.Label>
	    	  <Form.Control 
	    	  		type="text" 
	    	  		placeholder="First Name" 
	    	  		required
	    	  		onChange={e => setFirstName(e.target.value)}
	    	  		value={firstName}/>
	    	
	    	</Form.Group>
	    	<Form.Group className="mb-3" controlId="userLastName">
	    	  <Form.Label>Last Name</Form.Label>
	    	  <Form.Control 
	    	  		type="text" 
	    	  		placeholder="Last Name" 
	    	  		required
	    	  		onChange={e => setLastName(e.target.value)}
	    	  		value={lastName}/>
	    	</Form.Group>

	         <Form.Group className="mb-3" controlId="userEmail">
	           <Form.Label>Email address</Form.Label>
	           <Form.Control 
	           		type="email" 
	           		placeholder="Email" 
	           		required
	           		onChange={e => setEmail(e.target.value)}
	           		value={email}/>
	           <Form.Text className="text-muted">
	             We'll never share your email with anyone else.
	           </Form.Text>
	         </Form.Group>
	         <Form.Group className="mb-3" controlId="mobileNo">
	           <Form.Label>Mobile Number</Form.Label>
	           <Form.Control 
	           		type="number" 
	           		placeholder="0912345678" 
	           		required
	           		onChange={e => setMobileNo(e.target.value)}
	           		value={mobileNo}/>
	         </Form.Group>
	         <Form.Group className="mb-3" controlId="password1">
	           <Form.Label>Password</Form.Label>
	           <Form.Control 
	           		type="password" 
	           		placeholder="Password" 
	           		required
	           		onChange={e => setPassword1(e.target.value)}
	           		value={password1}/>
	         </Form.Group>

	         <Form.Group className="mb-3" controlId="password2">
	           <Form.Label>Verify Password</Form.Label>
	           <Form.Control 
	           		type="password" 
	           		placeholder="Password" 
	           		required
	           		onChange={e => setPassword2(e.target.value)}
	           		value={password2}/>
	         </Form.Group>


	        {isActive
	         ?
	         <Button variant="primary" type="submit">Submit</Button>
	         :
	         <Button variant="danger" type="submit" disabled>Submit</Button>
	    	}
	       </Form>
  	);
}

