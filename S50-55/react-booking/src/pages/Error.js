import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import { Navigate } from "react-router-dom";
import { Link } from 'react-router-dom';

import Banner from '../components/Banner';
import { Fragment } from 'react';

export default function Error(){


	const errorData ={
		title:"Page Not Found!",
		subTitle:"Go back to the ",
		routeName:"Homepage",
		route:"/"
	}
	return(

		<Fragment>
		<Banner bannerData = {errorData} />

		</Fragment>

	)
}