import { Fragment } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){
     const bannerData ={
          title:"Zuitt Coding Bootcamp",
          subTitle:"Opportunnities for everyone, everywhere",
          showButton:true

     }


	return(

    <Fragment>
         <Banner bannerData = {bannerData}/>
         <Highlights />
    </Fragment>

	)

}