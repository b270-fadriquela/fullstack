import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext, } from 'react';
import { Navigate, useLocation} from "react-router-dom";
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Login() {


	const { user, setUser, setUserName, userName} = useContext(UserContext);
	const [ email, setEmail ] = useState("");
	// const [ userName, setUserName ] = useState("");
	const [ password, setPassword ] = useState("");
	const [ isActive, setIsActive ] = useState(false);

	// const [ isLogin, setIsLogin ] = useState(false);


	

	useEffect(() => {
		if(password !== "" && email !== ""){
			setIsActive(true);
		}
		else
			setIsActive(false)
	}, [password, email])


	// window.dispatchEvent(new Event("logoutEvent"));

    const retrieveUserDetails = (token) => { 
            console.log(token);
            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
               headers: { 'Authorization': 'Bearer ' + token }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                setUser({
                    id:data._id,
                    isAdmin: data.isAdmin
                });

                setUserName({
                    name:data.firstName
                })


            });
    };

	function loginUser(e){
		e.preventDefault();
        
        //request REACT_APP_API_URL
       fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email:email,
                password:password
            })

        })
        .then(res => res.json())
        .then(data => {

            if(data.accessToken !== undefined){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken)


                Swal.fire({
                    title:"Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt"
                })
            }
            else{
                Swal.fire({
                    title:"Authentication Failed!",
                    icon: "error",
                    text: "Invalid Password/Email"
                })
            }
            
        })
        .catch(rejected => {
            console.log(rejected);
        });


		// setIsLogin(true);
		//setEmail(email);
		//localStorage.setItem("email",email);
		// window.dispatchEvent(new Event("loginEvent"));


		//let split = email.split("@");
		//setUserName ({name : split[0]});
		//setUser({email: localStorage.getItem('email')});
	}

    const welcomeData ={
       
        header:"Welcome Back! ",
        message:"Ready to learn more?",
        submessage:"Moving to courses in a few seconds"
    }

 	 return (

		(user.id !== null)
        ?
        <Navigate state={ welcomeData } to="/welcome"/ >
        :
        <Form onSubmit={(e) => loginUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }

        </Form>


  	);
}

