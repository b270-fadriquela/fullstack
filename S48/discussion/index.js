console.log("connect")

let posts = []


let count = 1;
let editIndex = -1;


document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	e.preventDefault();

	posts.push({

		id:count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	count++;

	alert("Successfully added");

	showPosts(posts);

});

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();

	if(editIndex < 0)
		return;

	alert("Post Edited");


	let objIndex = posts.findIndex((obj => obj.id == editIndex));

	posts[objIndex] =  {
			id:editIndex,
			title: document.querySelector("#txt-title-edit").value,
			body: document.querySelector("#txt-body-edit").value
		}


	document.querySelector("#txt-edit-id").value = "";
	document.querySelector("#txt-title-edit").value = "";
	document.querySelector("#txt-body-edit").value =  "";
		editIndex = -1;
		
	RefreshPostDisplay();

});




function showPosts(posts){

	RefreshPostDisplay();
}

function editPost(postId){

	editIndex = postId;

	let objIndex = posts.findIndex((obj => obj.id == postId));
	document.querySelector("#txt-edit-id").value = posts[objIndex].id;
	document.querySelector("#txt-title-edit").value =  posts[objIndex].title;
	document.querySelector("#txt-body-edit").value =  posts[objIndex].body;

}

function deletePost(postId){

	alert("Post Deleted" + postId);

	let objIndex = posts.findIndex((obj => obj.id == postId));
	posts.splice(objIndex,1);

	RefreshPostDisplay();
}


function RefreshPostDisplay(){

	let postEntries = "";
	posts.forEach((post) => {

		postEntries += `
		<div id="post"-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>
		`
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}