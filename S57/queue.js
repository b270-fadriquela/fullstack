let collection = [];

// Write the queue functions below.


function print(){
	return collection;
}

function enqueue(name){

	collection[collection.length] = name;
	return collection;
}

function dequeue(){

	let dequeuedCollection = [];
	for(const index in collection)
    {
    	if(index != 0)
		dequeuedCollection[dequeuedCollection.length] = collection[index];
    }
    collection = dequeuedCollection;
	return collection;
}

function front(){
	
	return collection[0] ;
}

function size(){

	let arraySize = 0;
	collection.forEach( () =>{
		arraySize++;
	} );

	return arraySize;
	
}

function isEmpty(){
		
	// let arraySize = 0;
	// collection.forEach( () =>{
	// 	arraySize++;
	// } );

	// return arraySize == 0;

	for(const index in collection)
    {
    	if(index > 0)
		return false;
    }
    return true

}


module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty

};